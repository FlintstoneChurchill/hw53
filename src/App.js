import React, {Component} from 'react';
import Form from './Todo/AddNewTask';
import TodoItem from './Todo/TodoItem';
import TodoList from './Todo/TodoList';
import './App.css';

class App extends Component {

    nextId = 4

    state = {
        items: [
            {text: 'adasdasdasda', id: 1},
            {text: 'zczczczczczc', id: 2},
            {text: 'qweqweqweqeq', id: 3}
        ],
        currentTask: ''
    };

    addItem = e => {
        e.preventDefault();
        document.getElementById('input-task').focus();
        let items = [...this.state.items];
        if (this.state.currentTask.length === 0) {
            this.setState({errorMessage: 'Введите текст'})
            return
        }
        const newItem = [{text: this.state.currentTask, id: this.nextId}]
        this.nextId++;
        this.setState({items: [...newItem, ...items], currentTask: '', errorMessage: ''});
    };

    removeItem = (e, id) => {
        e.preventDefault();
        let items = [...this.state.items];
        let item = items.findIndex(i => i.id === id);
        items.splice(item, 1);
        this.setState({items});
    };

    handleChange = e => {
        let currentTask = e.target.value;
        this.setState({currentTask});
    };

    render = () => {
        const errorMessage = this.state.errorMessage ? 
            <div style={{color: 'red'}}>{this.state.errorMessage}</div> : null
        return (
            <div>
                { errorMessage }
                <Form click={this.addItem} value={this.state.currentTask} change={this.handleChange} />
                <TodoList items={this.state.items} removeItem={this.removeItem} />
            </div>
        );
    }
}

export default App;
