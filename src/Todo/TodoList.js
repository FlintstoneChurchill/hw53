import React from 'react';
import TodoItem from './TodoItem';

const TodoList = ({items, removeItem}) => (
        <div className="TodoList">
        {items.map((item) => <TodoItem
                  message={item.text}
                  key={item.id}
                  remove={e => removeItem(e, item.id)} />)}
        </div>
    )

export default TodoList