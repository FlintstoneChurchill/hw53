import React from 'react';

const AskForUser = (props) => {
    return (
        <div>
        	<form onSubmit={props.click}>
        		<input id="input-task" value={props.value} type="text" onChange={props.change} />
        		<input type="submit" value="Add" />
        	</form>            
        </div>
    )
};

export default AskForUser;