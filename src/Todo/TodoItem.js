import React from 'react';

const TodoItem = (props) => {
    return (
        <div className="item">
            <p>{props.message} | <a href="" className="remove_item" onClick={props.remove}>Remove</a></p>
        </div>
    )
};

export default TodoItem;